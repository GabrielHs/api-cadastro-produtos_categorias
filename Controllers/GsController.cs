using Microsoft.AspNetCore.Mvc;
using ProductCatalog.Data;

namespace ProductCatalog.Controllers
{
    public class GsController : Controller
    {
        protected readonly StoreDataContext db;

        public StoreDataContext Db => db;
        public GsController(StoreDataContext context)
        {
            this.db = context;
        }
    }
}