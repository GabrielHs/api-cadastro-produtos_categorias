using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Data;
using ProductCatalog.Models;
using X.PagedList;

namespace ProductCatalog.Controllers
{
    public class CategoryController : GsController
    {
        public CategoryController(StoreDataContext context) : base(context)
        {
        }


        [Route("v1/categories")]
        [HttpGet]
        // [ResponseCache(Duration = 60)]
        public IActionResult Get([FromQuery] int page = 1, [FromQuery] string pesquisaTittle = "")
        {


            var categorias = Db.Categories.Where(c =>c.Tittle.Contains(pesquisaTittle)).AsNoTracking().ToList();
            var paginado = categorias.ToPagedList(page, 3);
            return Json(new { data = paginado, metadado = paginado.GetMetaData() });
        }



        [Route("v1/categories/{id}")]
        [HttpGet]
        public IActionResult Get(int id)
        {

            if (id == 0)
            {
                return NotFound(new { erro = "Paramentro inválido" });
            }

            try
            {
                var resultado = Db.Categories.AsNoTracking().FirstOrDefault(c => c.Id == id);
                return Json(new { ok = resultado });
            }
            catch (System.Exception ex)
            {

                throw new Exception("Erro ao Localizar item " + ex.InnerException.Message);

            }

        }


        [Route("v1/categories")]
        [HttpPost]
        public IActionResult Post([FromBody] Category category)
        {
            using (var transaction = Db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        Db.Add(category);
                        Db.SaveChanges();
                        transaction.Commit();

                        return Json(category);

                    }
                    return Json(ModelState.Keys.ToList());
                }
                catch (System.Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Erro ao salvar dados " + ex.InnerException.Message);

                }
            }

        }


        [Route("v1/categories")]
        [HttpPut]
        public IActionResult Put([FromBody] Category category)
        {

            using (var transaction = Db.Database.BeginTransaction())
            {
                try
                {
                    // 1 forma
                    // Db.Entry<Category>(category).State = EntityState.Modified;

                    // 2 forma
                    var categoryAtual = Db.Categories.FirstOrDefault(c => c.Id == category.Id);

                    categoryAtual = category;
                    Db.SaveChanges();
                    transaction.Commit();

                    return Json(new { ok = category, msg = "Atualizado com sucesso" });

                }
                catch (System.Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Erro ao atualizar dados " + ex.Message);
                }
            }

        }


        [Route("v1/categories")]
        [HttpDelete]
        public IActionResult Delete([FromBody] Category category)
        {

            using (var transaction = Db.Database.BeginTransaction())
            {
                try
                {

                    Db.Categories.Remove(category);

                    Db.SaveChanges();

                    transaction.Commit();

                    return Json(new { ok = "Excluido com sucesso" });

                }
                catch (System.Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception("Erro ao excluir dados " + ex.Message);
                }
            }

        }







    }
}