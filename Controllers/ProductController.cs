using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Data;
using ProductCatalog.Models;
using ProductCatalog.Repositories;
using ProductCatalog.ViewModels.ProductViewModels;

namespace ProductCatalog.Controllers
{
    public class ProductController : GsController
    {
        private readonly ProductReposity _repository;
        public ProductController(StoreDataContext context, ProductReposity repository) : base(context)
        {
            _repository = repository;
        }

        [Route("v1/products")]
        [HttpGet]
        public IEnumerable<ListProductViewModel> Get()
        {
            return _repository.Get(this);
        }

        [Route("v1/products/{id}")]
        [HttpGet]
        public Product Get(int id)
        {
            return _repository.Get(id, this);
        }

        [Route("v1/products")]
        [HttpPost]
        public IActionResult Post([FromBody] EditorProductViewModel model)
        {
            model.Validate();
            if (model.Invalid)
            {
                var erro = new ResultViewModel
                {
                    Success = false,
                    Message = "Não foi possivel cadastrar o produto",
                    Data = model.Notifications
                };
                return Json(erro);
            }

            using (var transaction = Db.Database.BeginTransaction())
            {
                try
                {
                    var product = new Product()
                    {
                        Title = model.Tittle,
                        CategoriaId = model.CategoriaId,
                        CreateDate = DateTime.Now,
                        Description = model.Description,
                        Image = model.Image,
                        LastUpdateDate = DateTime.Now,
                        Price = model.Price,
                        Quantity = model.Quantity
                    };

                    // Db.Products.Add(product);
                    // Db.SaveChanges();
                    _repository.Save(product, this);
                    transaction.Commit();

                    var retorno = new ResultViewModel
                    {
                        Success = true,
                        Message = "Produto Cadastro com sucesso",
                        Data = product
                    };

                    return Json(retorno);
                }
                catch (System.Exception e)
                {

                    transaction.Rollback();
                    throw new Exception("Erro ao salvar dados " + e.InnerException.Message);
                }


            }

        }

        [Route("v1/products")]
        [HttpPut]
        public IActionResult Put([FromBody] EditorProductViewModel model)
        {
            model.Validate();
            if (model.Invalid)
            {
                var erro = new ResultViewModel
                {
                    Success = false,
                    Message = "Não foi possivel cadastrar o produto",
                    Data = model.Notifications
                };
                return Json(erro);
            }


            try
            {
                var product = Db.Products.FirstOrDefault(p => p.Id == model.Id);

                product.Title = model.Tittle;
                product.CategoriaId = model.CategoriaId;
                product.CreateDate = DateTime.Now;
                product.Description = model.Description;
                product.Image = model.Image;
                product.LastUpdateDate = DateTime.Now;
                product.Price = model.Price;
                product.Quantity = model.Quantity;

                Db.SaveChanges();


                var retorno = new ResultViewModel
                {
                    Success = true,
                    Message = "Produto Cadastro com sucesso",
                    Data = product
                };

                return Json(retorno);
            }
            catch (System.Exception e)
            {

                throw new Exception("Erro ao salvar dados " + e.InnerException.Message);
            }





        }
    }
}