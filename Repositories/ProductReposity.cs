using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Controllers;
using ProductCatalog.Models;
using ProductCatalog.ViewModels.ProductViewModels;

namespace ProductCatalog.Repositories
{
    public class ProductReposity
    {
        public IEnumerable<ListProductViewModel> Get(GsController gs)
        {
            return gs.Db.Products
               .Include(c => c.Category)
               .Select(p => new ListProductViewModel()
               {
                   Id = p.Id,
                   Tittle = p.Title,
                   Price = p.Price,
                   Category = p.Category.Tittle,
                   CategoriaId = p.Category.Id
               })
                .AsNoTracking()
                .ToList();

        }

        public Product Get(int id, GsController gs)
        {
            return gs.Db.Products.FirstOrDefault(p => p.Id == id);
        }

        public void Save(Product product, GsController gs)
        {

            gs.Db.Products.Add(product);
            gs.Db.SaveChanges();
        }

        public void Update(Product product, GsController gs)
        {

            gs.Db.SaveChanges();
        }



    }
}