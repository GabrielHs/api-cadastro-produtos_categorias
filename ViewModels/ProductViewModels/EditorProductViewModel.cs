using Flunt.Notifications;
using Flunt.Validations;

namespace ProductCatalog.ViewModels.ProductViewModels
{
    public class EditorProductViewModel : Notifiable, IValidatable
    {
        public int Id { get; set; }

        public string Tittle { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public string Image { get; set; }

        public int CategoriaId { get; set; }

        public void Validate()
        {
            AddNotifications(
                new Contract()
                    .HasMaxLen(Tittle, 120, "Tittle", "O tituto deve conter ate 120 caracteres")
                    .HasMinLen(Tittle, 3, Tittle, "O titulo deve conter pelo menos 3 caracteres")
                    .IsGreaterThan(Price, 0, "Price", "O preço deve ser maior  que zerop")
            );
        }
    }
}