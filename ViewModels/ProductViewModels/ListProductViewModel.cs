namespace ProductCatalog.ViewModels.ProductViewModels
{
    public class ListProductViewModel
    {
        public int Id { get; set; }

        public string Tittle { get; set; }

        public decimal Price { get; set; }

        public int CategoriaId { get; set; }

        public string Category { get; set; }

        
    }
}